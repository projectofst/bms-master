#ifndef __TEMP_H_
#define __TEMP_H_

#include <stdint.h>
#include <stdbool.h>

#define MAX_VALID_TEMP_VOLT 27325 /**< [mv/10] */
#define MIN_VALID_TEMP_VOLT 1133 /**< [mv/10] */

extern const __prog__ int16_t ntc_curve[26192];

/**
 * @brief Converts ntc voltage to temperature
 */
#define convert_ntc_temp(v) ntc_curve[(v-1133)]

bool NTC_is_faulty(uint16_t voltage);
bool NTC_is_valid(uint16_t temp_fault_mask, uint16_t index);
uint16_t  count_valid_cell_NTC_in_stack(uint16_t temp_fault_mask);
#endif
