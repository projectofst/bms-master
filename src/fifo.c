#include <stdlib.h>
#include <stdint.h>
#include "fifo.h"
#include "lib_pic33e/can.h"

/* Init fifo with read and write pointers, message buffers and size of buffer */
void fifo_init(Fifo *fifo, CANdata *msgs, uint16_t size) {
	fifo->size = size;
	fifo->read_index = size-1;
	fifo->write_index = 0;
	fifo->msgs = msgs;
}

/* Append message to the fifo */
void append_fifo(Fifo *fifo, CANdata msg) {
	fifo->msgs[fifo->write_index] = msg;
	fifo->write_index = (fifo->write_index + 1) % fifo->size;
}

CANdata *pop_fifo(Fifo *fifo) {
	CANdata *aux;

	if (fifo_empty(fifo)) {
		return NULL;
	}
		
	fifo->read_index = (fifo->read_index + 1) % fifo->size;
	aux = &(fifo->msgs[fifo->read_index]);
	return aux;
}

bool fifo_empty(Fifo *fifo) {
	return (fifo->read_index + 1) % fifo->size == fifo->write_index;
}

uint16_t fifo_occupied(Fifo *fifo) {
	return (fifo->write_index - fifo->read_index) % fifo->size;
}

