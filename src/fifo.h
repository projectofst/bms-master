#ifndef __CAN_JOBS_
#define __CAN_JOBS_

#include <stdint.h>
#include "fifo.h"
#include "lib_pic33e/can.h"

typedef struct _Fifo {
	uint16_t size;
	uint16_t read_index;
	uint16_t write_index;
	CANdata *msgs;
} Fifo;


void fifo_init(Fifo *fifo, CANdata *msgs, uint16_t size);
void append_fifo(Fifo *fifo, CANdata msg);
CANdata *pop_fifo(Fifo *fifo);
bool fifo_empty(Fifo *fifo);
uint16_t fifo_occupied(Fifo *fifo);
#endif
