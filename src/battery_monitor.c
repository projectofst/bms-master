#include <string.h>

#include "lib_pic33e/timing.h"
#include "lib_pic33e/timer.h"
#include "lib_pic33e/utils.h"
#include "battery.h"
#include "LTC6811/LTC6811.h"
#include "temperature.h"
#include "signals.h"
#include "slave.h"
#include "temperature.h"
#include "master_state.h"

#include "can-ids/Devices/BMS_MASTER_CAN.h"


/**
 * @brief Change temperature mux address of all slaves to the passed address.
 */
void control_temp_mux_address(Battery* battery, uint16_t address){
    if (address >= 16) return;
    uint8_t to_send[battery->daisy_chain_B.n * LTC6811_REG_SIZE];
    unsigned int i = 0;

    for(i = 0; i < battery->daisy_chain_B.n; i++){
        battery->stacks[i].slave.BM.GPIO &= 0b10000;
        battery->stacks[i].slave.BM.GPIO |= (address & 0b1111);
        memcpy(to_send + (i * LTC6811_REG_SIZE), battery->stacks[i].slave.BM.CFGR,
                LTC6811_REG_SIZE);
    }
    broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC6811_REG_SIZE, to_send);
    return;
}


/**
 * @brief Changes discharge channels state to the contained in the battery
 * struct.
 */
void control_discharge_channels(Battery* battery){
    uint8_t to_send[BATTERY_SERIES_STACKS * LTC6811_REG_SIZE];
    unsigned int i = 0;

    for(i = 0; i < BATTERY_SERIES_STACKS; i++){
        battery->stacks[i].slave.BM.DCC =
            battery->stacks[i].slave.discharge_channel_state;
        memcpy(to_send + (i * LTC6811_REG_SIZE), battery->stacks[i].slave.BM.CFGR,
                LTC6811_REG_SIZE);
    }
    broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC6811_REG_SIZE, to_send);
    return;
}

/**
 * @brief Commands the LTC6811 to start converting the cells voltages
 */
void start_cell_voltages_conversion(Battery* battery){
    broadcast_poll(&battery->daisy_chain_B,
            ADCV(MD_NORMAL, DCP_DISCHARGE_NOT_PERMITTED, CH_ALL_CELLS));
    return;
}

/**
 * @brief Reads all cell voltages from the slaves and writes it on the battery
 * struct
 */
void get_cell_voltages(Battery* battery){
    uint16_t voltages[3 * BATTERY_SERIES_STACKS];
    uint16_t stack, cell;

    /*
     * Collect cells 0-2 (group A) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVA, LTC6811_REG_SIZE, (uint8_t*)voltages);

    for(stack = 0; stack < BATTERY_SERIES_STACKS; ++stack){
        for(cell = 0; cell < 3; ++cell){
            battery->stacks[stack].cell_pack[0 + cell].voltage =
                voltages[stack * 3 + cell];
        }
    }

    /*
     * Collect cells 3-5 (group B) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVB, LTC6811_REG_SIZE, (uint8_t*)voltages);

    for(stack = 0; stack < BATTERY_SERIES_STACKS; ++stack){
        for(cell = 0; cell < 3; ++cell){
            battery->stacks[stack].cell_pack[3 + cell].voltage =
                voltages[stack * 3 + cell];
        }
    }

    /*
     * Collect cells 6-8 (group C) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVC, LTC6811_REG_SIZE, (uint8_t*)voltages);

    for(stack = 0; stack < BATTERY_SERIES_STACKS; ++stack){
        for(cell = 0; cell < 3; ++cell){
            battery->stacks[stack].cell_pack[6 + cell].voltage =
                voltages[stack * 3 + cell];
        }
    }
    /*
     * Collect cells 9-11 (group D) from each LTC6811
     */
    broadcast_read(&battery->daisy_chain_B, RDCVB, LTC6811_REG_SIZE, (uint8_t*)voltages);

    for(stack = 0; stack < BATTERY_SERIES_STACKS; ++stack){
        for(cell = 0; cell < 3; ++cell){
            battery->stacks[stack].cell_pack[9 + cell].voltage =
                voltages[stack * 3 + cell];
        }
    }

    return;
}

/**
 * @brief Commands each LTC6811 to start converting a temperature (voltage on
 * GPIO5)
 *
 * @param temp The index of the temperature to measure.
 */
void start_temp_voltage_conversion(Battery* battery, uint16_t address){
    control_temp_mux_address(battery, address);
    broadcast_poll(&battery->daisy_chain_B, ADAX(MD_NORMAL, CHG_GPIO5));
    return;
}

/**
 * @brief Returns the correct pointer to write a temperature value.
 *
 * index is the temperature value id (0-11 for the cell temperatures, 12-13 for
 * the slave temperatures, 14-15 for the heatsink temperatures)
 *
 * slave is the slave number
 */
int16_t* get_temp_address(Battery* battery, uint16_t slave, uint16_t index){
    if (index < 12){
        return &battery->stacks[slave].cell_pack[index].temperature;
    } else if (index < 14){
        return &battery->stacks[slave].slave.slave_temperature[index - 12];
    } else if (index < 16){
        return &battery->stacks[slave].slave.heatsink_temperature[index - 14];
    }

    return NULL;
}

/**
 * @brief Writes value to the bit indicated by index of the mask pointed to by
 * temp_fault_mask
 */
void write_to_temp_fault_mask(uint16_t* temp_fault_mask, bool value, uint16_t index){
    *temp_fault_mask &= (~(1 << index));
    *temp_fault_mask |= (value << index);

    return;
}

/**
 * @brief Reads the temperature voltages from the slaves, checks faults,
 * converts and stores the temperatures
 */
void get_temperatures(Battery* battery, uint16_t address){
    uint16_t register_contents[3 * BATTERY_SERIES_STACKS];
    uint16_t slave; // iterator
    uint16_t temp_voltage;
    int16_t temperature;
    int16_t* temp_address;
    bool NTC_fault;
    uint16_t temp_index = NTC_mux_mapping[address];

    /* read the data from the slaves */
    broadcast_read(&battery->daisy_chain_B, RDAUXB, LTC6811_REG_SIZE,
            (uint8_t*)register_contents);

    for(slave = 0; slave < BATTERY_SERIES_STACKS; ++slave){
        /*
         * Get the contents of the G5V field in the LTC AVBR register (AVBR2 and
         * AVBR3 bytes);
         */
        temp_voltage = register_contents[slave * 3 + 1];

        /* check if the measurement is valid */
        NTC_fault = NTC_is_faulty(temp_voltage);
        write_to_temp_fault_mask(&battery->stacks[slave].slave.temp_fault_mask,
                NTC_fault, temp_index);

        if (!NTC_fault){
            /* convert voltage to temperature */
            temperature = convert_ntc_temp(temp_voltage); // ºC/10

            /* store the temperature value */
            temp_address = get_temp_address(battery, slave, temp_index);
            *temp_address = temperature;
        }
    }

    return;
}

void calculate_summary_values(Battery* battery){
    uint16_t lowest_V = UINT16_MAX;
    uint16_t lowest_V_cell = 0;
    uint16_t highest_V = 0;
    uint16_t highest_V_cell = 0;
    uint32_t total_V = 0;

    uint16_t bat_lowest_V = UINT16_MAX;
    uint16_t bat_lowest_V_cell[2] = {0};
    uint16_t bat_highest_V = 0;
    uint16_t bat_highest_V_cell[2] = {0};
    uint32_t bat_total_V = 0;

    int16_t lowest_T = INT16_MAX;
    int16_t lowest_T_cell = 0;
    int16_t highest_T = INT16_MIN;
    int16_t highest_T_cell = 0;
    int32_t total_T = 0;
    uint16_t stack_valid_NTC = 0;

    int16_t bat_lowest_T = INT16_MAX;
    int16_t bat_lowest_T_cell[2] = {0};
    int16_t bat_highest_T = INT16_MIN;
    int16_t bat_highest_T_cell[2] = {0};
    int32_t bat_total_T = 0;
    uint16_t bat_valid_NTC = 0;

    uint16_t stack_i, cell_i; // iterators

    Stack* stack; Cell* cell;

    for(stack_i = 0; stack_i < BATTERY_SERIES_STACKS; ++stack_i){
        lowest_V = UINT16_MAX;
        total_V = 0;
        highest_V = 0;
        lowest_T = INT16_MAX;
        total_T = 0;
        highest_T = INT16_MIN;

        stack = &battery->stacks[stack_i];

        /* calculate stack values */
        for(cell_i = 0; cell_i< BATTERY_SERIES_CELLS_PER_STACK; ++cell_i){
            cell = &stack->cell_pack[cell_i];

            /* voltage */
            total_V += cell->voltage;
            if(cell->voltage < lowest_V){
                lowest_V = cell->voltage;
                lowest_V_cell = cell_i;
            }
            if(cell->voltage > highest_V){
                highest_V = cell->voltage;
                highest_V_cell = cell_i;
            }
            /* temperature */
            if (NTC_is_valid(stack->slave.temp_fault_mask, cell_i)){ // only use valid NTC
                total_T += cell->temperature;
                if(cell->temperature < lowest_T){
                    lowest_T = cell->temperature;
                    lowest_T_cell = cell_i;
                }
                if(cell->temperature > highest_T){
                    highest_T = cell->temperature;
                    highest_T_cell = cell_i;
                }
            }
        }

        /* store summary voltage values */
        stack->total_voltage = total_V/10;
        stack->mean_voltage = total_V/BATTERY_SERIES_CELLS_PER_STACK;
        stack->max_voltage = highest_V;
        stack->max_voltage_cell = highest_V_cell;
        stack->min_voltage = lowest_V;
        stack->min_voltage_cell = lowest_V_cell;
        /* store summary temperature values */
        stack_valid_NTC = count_valid_cell_NTC_in_stack(stack->slave.temp_fault_mask);
        stack->n_valid_cell_NTC = stack_valid_NTC;
        stack->mean_temperature = total_T/stack_valid_NTC;
        stack->max_temperature = highest_T;
        stack->max_temperature_cell = highest_T_cell;
        stack->min_temperature = lowest_T;
        stack->min_temperature_cell = lowest_T_cell;

        /* calculate battery values */
        /* voltage */
        bat_total_V += total_V;
        if(lowest_V < bat_lowest_V){
            bat_lowest_V = lowest_V;
            bat_lowest_V_cell[0] = stack_i;
            bat_lowest_V_cell[1] = lowest_V_cell;
        }
        if(highest_V > bat_highest_V){
            bat_highest_V = highest_V;
            bat_highest_V_cell[0] = stack_i;
            bat_highest_V_cell[1] = highest_V_cell;
        }
        /* temperature */
        bat_total_T += total_T;
        bat_valid_NTC += stack_valid_NTC;
        if(lowest_T < bat_lowest_T){
            bat_lowest_T = lowest_T;
            bat_lowest_T_cell[0] = stack_i;
            bat_lowest_T_cell[1] = lowest_T_cell;
        }
        if(highest_T > bat_highest_T){
            bat_highest_T = highest_T;
            bat_highest_T_cell[0] = stack_i;
            bat_highest_T_cell[1] = highest_T_cell;
        }
    }

    /* store battery summary values */
    battery->total_voltage = bat_total_V/100;
    battery->mean_voltage = bat_total_V/BATTERY_TOTAL_SERIES_CELLS;
    battery->min_voltage = bat_lowest_V;
    battery->min_voltage_cell[0] = bat_lowest_V_cell[0];
    battery->min_voltage_cell[1] = bat_lowest_V_cell[1];
    battery->max_voltage = bat_highest_V;
    battery->max_voltage_cell[0] = bat_highest_V_cell[0];
    battery->max_voltage_cell[1] = bat_highest_V_cell[1];

    battery->mean_temperature = bat_total_T/bat_valid_NTC;
    battery->min_temperature = bat_lowest_T;
    battery->min_temperature_cell[0] = bat_lowest_T_cell[0];
    battery->min_temperature_cell[1] = bat_lowest_T_cell[1];
    battery->max_temperature = bat_highest_T;
    battery->max_temperature_cell[0] = bat_highest_T_cell[0];
    battery->max_temperature_cell[1] = bat_highest_T_cell[1];

    return;
}

void check_battery_limits(Battery* battery){
    uint16_t stack_i; // iterator
    /* Check voltage limits */
    if(battery->max_voltage > CELL_LIMIT_MAX_V){
		set_ams_ok(battery, 0, TS_OFF_REASON_OVERVOLTAGE);
		return;
    }

    if(battery->min_voltage < CELL_LIMIT_MIN_V){
		set_ams_ok(battery, 0, TS_OFF_REASON_UNDERVOLTAGE);
		return;
    }

    /* Check Temperature limits */
    if(battery->max_temperature > CELL_LIMIT_MAX_T){
		set_ams_ok(battery, 0, TS_OFF_REASON_OVERTEMPERATURE);
		return;
    }
    if(battery->min_temperature < CELL_LIMIT_MIN_T){
		set_ams_ok(battery, 0, TS_OFF_REASON_UNDERTEMPERATURE);
		return;
    }
    for (stack_i = 0; stack_i < BATTERY_SERIES_STACKS; ++stack_i){
        if(battery->stacks[stack_i].n_valid_cell_NTC < STACK_LIMIT_MIN_CELL_NTC){
            set_ams_ok(battery, 0, TS_OFF_REASON_MIN_CELL_LTC);
            return;
        }

    }
    /* Check Current limits */
    if(battery->current > BAT_LIMIT_MAX_I_DISCH){
		set_ams_ok(battery, 0, TS_OFF_REASON_OVERCURRENT_OUT);
		return;
    }
    if(battery->current < BAT_LIMIT_MAX_I_CHRG){
		set_ams_ok(battery, 0, TS_OFF_REASON_OVERCURRENT_IN);
		return;
    }
	set_ams_ok(battery, 1, TS_OFF_REASON_NO_ERROR);
    return;
}

void set_stack_discharge_channels(Stack *stack, uint16_t min_voltage, bool charging) {
	Slave *slave = &stack->slave;
	int i;
	for (i=0; i<BATTERY_SERIES_CELLS_PER_STACK; i++) {
		if (stack->cell_pack[i].voltage - min_voltage > BAT_CHARGING_DELTA &&
				slave->heatsink_temperature[0] < SLAVE_MAX_HEATSINK_TEMP &&
				slave->heatsink_temperature[1] < SLAVE_MAX_HEATSINK_TEMP &&
				charging) {
			slave->discharge_channel_state |= 1 << i;
		}
		else {
			slave->discharge_channel_state &= ~(1 << i);
		}
	}
}

void set_discharge_channels(Battery *battery, MasterState master_state) {
	Stack *stack;
	int i;
	for (i=0; i<BATTERY_SERIES_STACKS; i++) {
		stack = &(battery->stacks[i]);
		set_stack_discharge_channels(stack, battery->min_voltage, master_state.charging);
	}
}

void battery_monitor_routine(Battery* battery, MasterState master_state){
    LED4 = 1;


    /* Measure all cell voltages */
    start_cell_voltages_conversion(battery);
    __delay_us(T_CONV_CELL_ALL_7K);
    get_cell_voltages(battery);

    /* Measure all cell temperatures */
    unsigned int address = 0;
    for (address = 0; address < 16; ++address){
        start_temp_voltage_conversion(battery, address);
        __delay_us(T_CONV_GPIO_ONE_7K);
        get_temperatures(battery, address);
    }

    /* Calculate battery summary values from the newly collected values*/
    calculate_summary_values(battery);

    /* Check if the values are within the limits, set AMS_OK to 0 if needed */
    check_battery_limits(battery);

	set_discharge_channels(battery, master_state);

    LED4 = 0;

    return;
}

void enable_daisy_chain_B_comm(void){
    CS_B = 0;
    return;
}

void disable_daisy_chain_B_comm(void){
    CS_B = 1;
    return;
}

void daisy_chain_B_watchdog_init(void){
    config_timer2_us(MIN_T_IDLE, 4); // timer configured to 4.3ms with priority 4
    return;
}
void daisy_chain_B_watchdog_enable(void){
    enable_timer2();
    return;
}
void daisy_chain_B_watchdog_reset(void){
    reset_timer2();
    return;
}
void daisy_chain_B_watchdog_disable(void){
    disable_timer2();
    return;
}

void timer2_callback(){
    // do nothing
    return;
}
void daisy_chain_B_init(LTC6811_daisy_chain* daisy_chain_B){
    daisy_chain_B->n = BATTERY_SERIES_STACKS;
    daisy_chain_B->core_state = CORE_SLEEP_STATE;
    daisy_chain_B->interface_state = ISOSPI_IDLE_STATE;
    daisy_chain_B->enable_comm = &enable_daisy_chain_B_comm;
    daisy_chain_B->disable_comm = &disable_daisy_chain_B_comm;
    daisy_chain_B->reset_interface_watchdog = &daisy_chain_B_watchdog_reset;

    return;
}

/*
 * @brief Ensures a valid communication with the slaves
 *
 * Wakes up the daisy chain, writes to to the configuration registers and
 * reads from them to check is the write was successful, thus checking
 * valid communication with the slaves.
 */
void start_communication_with_slaves(Battery* battery){
    uint16_t slave, i; // iterators
    bool slave_comm_OK = 0;
    uint16_t slave_comm_error = 0;
#if 0
    struct CFGR{
        uint8_t ADCOPT : 1;
        uint8_t DTEN   : 1;
        uint8_t REFON  : 1;
        uint8_t GPIO    : 5;
        uint32_t : 24;
        uint16_t DCC    : 12;
        uint8_t DCTO    : 4;
    }default_CFGR = {
        .ADCOPT = 0,
        .DTEN = 0,
        .REFON = 1,
        .GPIO = 0x1F,
        .DCC = 0,
        .DCTO = 0
    };
#endif
    uint8_t default_CFGR[6] = {0b11111100, 123, 123, 123, 0, 0};
    uint8_t CFGR_contents[6 * BATTERY_SERIES_STACKS];
    uint8_t receive[6 * BATTERY_SERIES_STACKS];

    TRIS_CS_B = 0; // set slave select pin as output

    /* Wake up the slaves  from sleep */
    wake_up_LTC6811_from_SLEEP(&battery->daisy_chain_B);

    /* Fill the array to send */
    for(slave = 0; slave < BATTERY_SERIES_STACKS; ++slave){
        memcpy(&CFGR_contents[6*slave], &default_CFGR, LTC6811_REG_SIZE);
    }

    while(!slave_comm_OK){
        slave_comm_error = 0;
        broadcast_write(&battery->daisy_chain_B, WRCFGA, LTC6811_REG_SIZE, CFGR_contents);

        if(
            broadcast_read(&battery->daisy_chain_B, RDCFGA, LTC6811_REG_SIZE,
                receive)
        ) slave_comm_error++;

        /* check the received contents are equal to the sent */
        for(slave = 0; slave < BATTERY_SERIES_STACKS; ++slave){
            // ignore first byte because of GPIO read being different from
            // write
            for(i = 1; i < 6; ++i){
                if(receive[6*slave + i] != CFGR_contents[6*slave + i])
                    slave_comm_error++;
            }
        }

        if(slave_comm_error == 0)slave_comm_OK = 1;
        slave_comm_OK = 1;
        /* else maybe send the count for debug */
    }

    // update the battery structure with the written CFGR contents
    for(slave = 0; slave < BATTERY_SERIES_STACKS; ++slave){
        memcpy(&battery->stacks[slave].slave.BM.CFGR, &default_CFGR,
                LTC6811_REG_SIZE);
    }
    return;
}
