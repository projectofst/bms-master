// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF				// General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF				// General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF				// General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC				// Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF				// Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = XT				// Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF			// OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF			// Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD			// Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS512		// Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128			// Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON				// PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF				// Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON				// Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR128			// Power-on Reset Timer Value Select bits (128ms)
#pragma config BOREN = ON				// Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF			// Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE				// ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF				// Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF				// JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF				// Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF				// Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF				// Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"

#include "lib_pic33e/timing.h"
#include "lib_pic33e/usb_lib.h"
#include "lib_pic33e/external.h"
#include "lib_pic33e/SPI.h"
#include "lib_pic33e/can.h"
#include "lib_pic33e/timer.h"
#include "LTC6811/LTC6811.h"
#include "battery.h"
#include "battery_monitor.h"
#include "signals.h"
#include "fifo.h"
#include "master_state.h"
#include "ts_turn_on.h"
#include "communication.h"

#define VERBOSE_CAN_SIZE 400
#define NORMAL_CAN_SIZE  64

uint32_t seconds;
bool status_flag = 0;
void timer1_callback() {
	seconds++;
	status_flag = true;
}


typedef struct _external_signals {
	union {
		struct {
			bool ams_ok                 : 1;
			bool imd_ok                 : 1;
			bool imd_latch              : 1;
			bool ams_latch              : 1;
			bool air_positive			: 1;
			bool air_negative			: 1;
			bool precharge              : 1;
			bool discharge              : 1;
			bool sc_dcu_imd             : 1;
			bool sc_imd_ams             : 1;
			bool sc_ams_dcu             : 1;
			bool sc_tsms_relays         : 1;
			bool shutdown_above_minimum : 1;
			bool shutdown_open          : 1;
		};
		uint16_t status;
	};
} ExternalSignals;


ExternalSignals external_sigs;

inline void poll_signals(Battery *battery) {
	battery->imd_ok				    = Det_IMD_OK;
	battery->imd_latch              = Det_IMD_latch;
	battery->ams_latch              = Det_AMS_latch;
	battery->air_positive		    = Det_AIR_Plus^1;
	battery->air_negative		    = Det_AIR_Minus^1;
	battery->precharge              = Det_PreChK^1;
	battery->discharge              = Det_DisChK^1;
	battery->sc_dcu_imd             = Det_Shutdown_DCU_IMD;
	battery->sc_imd_ams			    = Det_Shutdown_IMD_AMS;
	battery->sc_ams_dcu             = Det_Shutdown_AMS_DCU;
	battery->sc_tsms_relays         = Det_Shutdown_TSMS_Relays;
	battery->shutdown_above_minimum = Shutdown_Above_Min;
	battery->shutdown_open			= Shutdown_Opening_Sig;

	return;
}

MasterState master_state = {
		.fans = 0,
		.verbose = 0,
		.charging = 0,
		.car_ts = 0,
};

//TODO: implement these functions
bool check_ams_ok() {
	return true;
}

void set_ams_ok(Battery *battery, bool intended_ams_ok, MASTER_MSG_TS_OFF_Reason reason) {
	uint16_t ams_ok = check_ams_ok();
	if (intended_ams_ok && !ams_ok) {
		return;
	}

	battery->ams_ok = intended_ams_ok;
	battery->ams_off_reason = reason;
	return;
}

bool check_ts_on() {
	return true;
}

void set_ts_on(bool inteded_ts_on) {
	return;
}

TS_TURN_ON ts_turn_on_state = TS_WAIT_FOR_COMMAND;

void ts_turn_off_sequence(void) {
	AIR_Plus_Control = 0;
	__delay_ms(RELAY_RELEASE_TIME);
	PreChK_Control = 0;
	__delay_ms(RELAY_RELEASE_TIME);
	DisChK_Control = 0;
	__delay_ms(RELAY_OPERATION_TIME + RELAY_BOUNCE_TIME);
	AIR_Minus_Control = 0;

	return;
}

void ts_turn_on_sequence(Battery *battery) {

	switch (ts_turn_on_state) {
		case TS_WAIT_FOR_COMMAND:
			if (!battery->sc_tsms_relays || !battery->ams_ok) {
				master_state.ts_turn_on = 0;
			}
			else if (master_state.ts_turn_on) {
				ts_turn_on_state = TS_INITIAL_STATE;
			}
			break;
		case TS_INITIAL_STATE:
			if (!battery->sc_tsms_relays) {
				master_state.ts_turn_on = 0;
				ts_turn_on_state = TS_ERROR_STATE;
			}
			else if (master_state.ts_turn_on && battery->ams_ok) {
				DisChK_Control = 1;
				AIR_Minus_Control = 1;
				__delay_ms(RELAY_OPERATION_TIME+RELAY_BOUNCE_TIME);
				ts_turn_on_state = TS_WAIT_FOR_PRECHARGE;
				PreChK_Control = 1;
			}
			break;
		case TS_WAIT_FOR_PRECHARGE:
			if (!battery->ams_ok || !master_state.ts_turn_on || !battery->sc_tsms_relays) {
				master_state.ts_turn_on = 0;
				ts_turn_on_state = TS_ERROR_STATE;
				return;
			}
			if (PreCh_End_Sig == 1) {
				AIR_Plus_Control = 1;
				__delay_ms(RELAY_OPERATION_TIME + RELAY_BOUNCE_TIME);
				PreChK_Control = 0;
				ts_turn_on_state = TS_END_STATE;
			}
			break;
		case TS_END_STATE:
			if (battery->ams_ok == 0 || master_state.ts_turn_on == 0 || !battery->sc_tsms_relays) {
				master_state.ts_turn_on = 0;
				ts_turn_on_state = TS_ERROR_STATE;
				return;
			}
			break;
		case TS_ERROR_STATE:
			ts_turn_off_sequence();
			ts_turn_on_state = TS_WAIT_FOR_COMMAND;
			break;
	}
}

void send_ts_reason_message(Battery *battery, Fifo *fifo) {
	CANdata msg;

	msg.dev_id = DEVICE_ID_BMS_MASTER;
	msg.msg_id = MSG_ID_MASTER_TS_STATE;

	msg.dlc = 2;

	msg.data[0] = battery->ams_off_reason;
	
	append_fifo(fifo, msg);
}

void send_ts_turn_on(Fifo *fifo) {
	CANdata msg;
	
	msg.dev_id = DEVICE_ID_BMS_MASTER;
	msg.msg_id = CMD_ID_COMMON_TS;

	msg.dlc = 2;

	msg.data[0] = TS_MASTER_ON;

	append_fifo(fifo, msg);
}

int main(void)
{
	config_outputs();

	LED3 = 0;
	LED4 = 0;
	LED5 = 0;

	Battery battery;
    battery_init(&battery);
	set_ams_ok(&battery, 1, TS_OFF_REASON_NO_ERROR);

	Fifo _verbose_can_fifo;
	Fifo _normal_can_fifo;
	Fifo *verbose_can_fifo = &_verbose_can_fifo;
	Fifo *normal_can_fifo = &_normal_can_fifo;

	CANdata verbose_msgs[VERBOSE_CAN_SIZE];
	CANdata normal_msgs[NORMAL_CAN_SIZE];

	fifo_init(verbose_can_fifo, verbose_msgs, VERBOSE_CAN_SIZE);
	fifo_init(normal_can_fifo, normal_msgs, NORMAL_CAN_SIZE);

	CANdata status_msg;
	CANdata charging_msg;
    /*
     * Configuration of the modules and the pin assignment
     */

    /* Configuration of the SPI module for slave communication */
    SPI_Peripheral_Configuration spi_config = {
        .enabled            = 1,
        .stop_in_idle       = 0,
        .disable_SCK_pin    = 0,
        .disable_SDO_pin    = 0,
        .transfer_mode      = SPI_TRANSFER_MODE_8BIT,
        .data_sample_phase  = SPI_DATA_SAMPLE_PHASE_MIDDLE,
        .SPI_mode           = SPI_MODE_3,
        .enable_SS_pin      = 0,
        .enable_master_mode = 1,
        .baudrate           = 1000000
    };

    config_SPI2(spi_config);

	do_the_pps();
	CANconfig can_config;
	default_can_config(&can_config);
	//config_master_filters(&can_config);
	can_config.number_masks = 1;
	can_config.filter_masks[0] = 0b00000011111;
	can_config.number_filters = 2;
	can_config.filter_sids[0] = 31;
	can_config.filter_sids[1] = 10;
	can_config.filter_mask_source[0] = 0;
	can_config.filter_mask_source[1] = 0;
	config_can1(&can_config);

	CANdata reset_msg = make_reset_msg(DEVICE_ID_BMS_MASTER, RCON);
	send_can1(reset_msg);

	/* Shutdown open external interrupt */
	//config_external0(RISING, 4);

	/* IMD external interrupt */
	config_timer1(1000, 7);

    /*
     * Variables
     */

    /*
     * Initial code run
     */
    start_communication_with_slaves(&battery);


    /*
     * Main Loop
     */

	while (1) {
        /*
         * Data gathering
         */

        battery_monitor_routine(&battery, master_state);

		/* Update battery status */
		poll_signals(&battery);

		/* Process data */

		/* Actuation */

		AMS_OK = battery.ams_ok;

		/*
         * When there is a request to turn ON the TS and AMS is ok we start the
		 * TS turn ON state machine
		 */
		if (master_state.car_ts) {
			master_state.car_ts = 0;
			if (battery.ams_ok && !master_state.ts_turn_on) {
				master_state.ts_turn_on = 1;
			}
			else {
				battery.ams_off_reason = TS_OFF_REASON_DASH_BUTTON;
				master_state.ts_turn_on = 0;
			}
		}

		ts_turn_on_sequence(&battery);

		//Actuation
		FansR_Control = master_state.fans;
		FansF_Control = master_state.fans;

		control_discharge_channels(&battery);
        /*
         * Communication
         */

		if (ts_turn_on_state == TS_ERROR_STATE) {
			send_ts_reason_message(&battery, normal_can_fifo);
		}
		if (ts_turn_on_state == TS_INITIAL_STATE) {
			send_ts_turn_on(normal_can_fifo);	
		}
		if (status_flag) {
			send_battery_info_messages(&battery, normal_can_fifo);
		}
        if (master_state.verbose && status_flag){
            send_slave_and_cell_messages(&battery, verbose_can_fifo);
        }
		/* Make status message */
		if (status_flag) {
			compose_status_message(&status_msg, battery, master_state);
			append_fifo(normal_can_fifo, status_msg);
			status_flag = false;
		}

		if (master_state.charging) {
			compose_charging_message(&charging_msg, master_state);
			append_fifo(normal_can_fifo, charging_msg);
		}

		while(can_dispatch(normal_can_fifo, verbose_can_fifo));
		while(can_receive_handler(&battery, &master_state, &ts_turn_on_state));

        ClrWdt();

	}
	return 0;
}
