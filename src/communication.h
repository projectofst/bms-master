#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include <stdbool.h>
#include "lib_pic33e/can.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"
#include "can-ids/Devices/BMS_VERBOSE_CAN.h"
#include "battery.h"
#include "fifo.h"
#include "master_state.h"
#include "ts_turn_on.h"

CANdata compose_master_ts_message(bool state, MASTER_MSG_TS_OFF_Reason off_reason);
CANdata compose_master_energy_meter_message(const Battery* battery);
CANdata compose_cell_voltage_info_message(const Battery* battery);
CANdata compose_cell_temperature_info_message(const Battery* battery);
CANdata compose_master_imd_error_message();
CANdata compose_master_ams_error_message();
CANdata compose_master_sc_open_message();
CANdata compose_cell_info_message(const Cell* cell);
CANdata compose_slave_info_1_message(const Stack* stack);
CANdata compose_slave_info_2_message(const Stack* stack);
CANdata compose_slave_info_3_message(const Stack* stack);
void send_slave_and_cell_messages(const Battery* battery, Fifo* can_fifo);
void send_battery_info_messages(const Battery* battery, Fifo* can_fifo);
void compose_status_message(CANdata *msg, Battery battery, MasterState master_state);
uint16_t can_dispatch(Fifo *fifo1, Fifo *fifo2);
int can_receive_handler(Battery *battery, MasterState *master_state, TS_TURN_ON *ts_turn_on_state);

#endif
