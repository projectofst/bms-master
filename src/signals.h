#ifndef __SIGNALS_H
#define __SIGNALS_H

#include <xc.h>

/* config */
/* outputs */
#define TRIS_AMS_OK						TRISBbits.TRISB2
#define TRIS_GPIO3						TRISBbits.TRISB7
#define TRIS_GPIO4						TRISBbits.TRISB8
#define TRIS_CONVST_EM					TRISDbits.TRISD5
#define TRIS_RESET_EM					TRISDbits.TRISD6
#define TRIS_AIR_Plus_Control			TRISDbits.TRISD8
#define TRIS_AIR_Minus_Control			TRISDbits.TRISD9
#define TRIS_PreChK_Control  			TRISDbits.TRISD10
#define TRIS_DisChK_Control				TRISDbits.TRISD11
#define TRIS_FansR_Control				TRISEbits.TRISE1
#define TRIS_FansF_Control				TRISEbits.TRISE3
#define TRIS_GPIO0						TRISEbits.TRISE4
#define TRIS_GPIO1						TRISEbits.TRISE5
#define TRIS_GPIO2						TRISEbits.TRISE6
#define TRIS_LED3						TRISEbits.TRISE4
#define TRIS_LED4						TRISEbits.TRISE5
#define TRIS_LED5						TRISEbits.TRISE6
#define TRIS_CS_B                       TRISEbits.TRISE7

#define TRIS_Det_IMD_latch				TRISBbits.TRISB3
#define TRIS_Det_AMS_latch				TRISBbits.TRISB4
#define TRIS_HV_Present_Sig				TRISBbits.TRISB9
#define TRIS_Det_AIR_Plus				TRISBbits.TRISB10
#define TRIS_Det_AIR_Minus				TRISBbits.TRISB11
#define TRIS_Det_PreChK					TRISBbits.TRISB12
#define TRIS_Det_DisChK					TRISBbits.TRISB13
#define TRIS_PreCh_End_Sig				TRISBbits.TRISB14
#define TRIS_Shutdown_Opening_Sig		TRISDbits.TRISD0
#define TRIS_BUSY_EM					TRISDbits.TRISD7
#define TRIS_Shutdown_Above_Min			TRISCbits.TRISC13
#define TRIS_Det_Shutdown_IMD_AMS		TRISEbits.TRISE0
#define TRIS_Det_Shutdown_DCU_IMD		TRISEbits.TRISE2
#define TRIS_Det_Shutdown_TSMS_Relays	TRISFbits.TRISF0
#define TRIS_Det_Shutdown_AMS_DCU		TRISFbits.TRISF1
#define TRIS_Det_IMD_OK					TRISGbits.TRISG9


/* outputs */
#define AMS_OK						LATBbits.LATB2
#define GPIO3						LATBbits.LATB7
#define GPIO4						LATBbits.LATB8
#define CONVST_EM					LATDbits.LATD5
#define RESET_EM					LATDbits.LATD6
#define AIR_Plus_Control			LATDbits.LATD8
#define AIR_Minus_Control			LATDbits.LATD9
#define PreChK_Control  			LATDbits.LATD10
#define DisChK_Control				LATDbits.LATD11
#define FansR_Control				LATEbits.LATE1
#define FansF_Control				LATEbits.LATE3
#define GPIO0						LATEbits.LATE4
#define GPIO1						LATEbits.LATE5
#define GPIO2						LATEbits.LATE6
#define LED3						LATEbits.LATE4
#define LED4						LATEbits.LATE5
#define LED5						LATEbits.LATE6
#define CS_B                        LATEbits.LATE7

/* inputs */
#define Det_IMD_latch				PORTBbits.RB3
#define Det_AMS_latch				PORTBbits.RB4
#define HV_Present_Sig				PORTBbits.RB9
#define Det_AIR_Plus				PORTBbits.RB10
#define Det_AIR_Minus				PORTBbits.RB11
#define Det_PreChK					PORTBbits.RB12
#define Det_DisChK					PORTBbits.RB13
#define PreCh_End_Sig				PORTBbits.RB14
#define Shutdown_Opening_Sig		PORTDbits.RD0
#define BUSY_EM						PORTDbits.RD7
#define Shutdown_Above_Min			PORTCbits.RC13
#define Det_Shutdown_IMD_AMS		PORTEbits.RE0
#define Det_Shutdown_DCU_IMD		PORTEbits.RE2
#define Det_Shutdown_TSMS_Relays	PORTFbits.RF0
#define Det_Shutdown_AMS_DCU		PORTFbits.RF1
#define Det_IMD_OK					PORTGbits.RG9

void do_the_pps(void);
void config_outputs(void);
#endif
