/* Include Guard */
#ifndef _BATTERY_MONITOR_H_
#define _BATTERY_MONITOR_H_

#include "master_state.h"

/**
 * @brief Change temperature mux address of all slaves to the passed address.
 */
void control_temp_mux_address(Battery* battery, uint16_t address);
/**
 * @brief Changes discharge channels state to the contained in the battery
 * struct.
 */
void control_discharge_channels(Battery* battery);

/**
 * @brief Commands the LTC6811 to start converting the cells voltages
 */
void start_cell_voltages_conversion(Battery* battery);
/**
 * @brief Reads all cell voltages from the slaves and writes it on the battery
 * struct
 */
void get_cell_voltages(Battery* battery);


/**
 * @brief Gets cell data from the slaves
 */
void battery_monitor_routine(Battery* battery, MasterState master_state);

/**
 * @brief Initializes the battery's daisy chain B struct
 */
void daisy_chain_B_init(LTC6811_daisy_chain* daisy_chain_B);

/*
 * @brief Ensures a valid communication with the slaves
 *
 * Wakes up the daisy chain, writes to to the configuration registers and
 * reads from them to check is the write was successful, thus checking
 * valid communication with the slaves.
 */
void start_communication_with_slaves(Battery* battery);

#endif /* End include guard */
