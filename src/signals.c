#include "lib_pic33e/pps.h"

#include "signals.h"

void do_the_pps(void) {
	PPSUnLock;

	/* CAN 1 */
	#ifdef TESTING
	PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
	PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
	#endif

	#ifndef TESTING
	PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP65);
	PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI62);
	#endif

	/* SPI 1 for Energy Meter */
	PPSInput(IN_FN_PPS_SDI1, IN_PIN_PPS_RP66);
	PPSOutput(OUT_FN_PPS_SCK1, OUT_PIN_PPS_RP67);
	PPSInput(IN_FN_PPS_SCK1, IN_PIN_PPS_RP67);

	/* Output Compare for Fan Control */
	/* Unmappable
	 * PPSOutput(OUT_FN_PPS_OC1, OUT_PIN_PPS_RPI81);
	 * PPSOutput(OUT_FN_PPS_OC2, OUT_PIN_PPS_RPI83);
	 */

	/* Secundary SPI module */
	PPSInput(IN_FN_PPS_SDI3, IN_PIN_PPS_RP101);
	PPSOutput(OUT_FN_PPS_SDO3, OUT_PIN_PPS_RP99);
	PPSOutput(OUT_FN_PPS_SCK3, OUT_PIN_PPS_RP100);
	PPSInput(IN_FN_PPS_SCK3, IN_PIN_PPS_RP100);

	/* Shutdown openning sig internal interrupt */
	PPSInput(IN_FN_PPS_INT1, IN_PIN_PPS_RP64);

	/* IMD ok signal interrupt */
	PPSInput(IN_FN_PPS_INT2, IN_PIN_PPS_RPI121);

	PPSLock;

	return;
}

void config_outputs(void) {
	TRIS_AMS_OK						= 0;
	TRIS_GPIO3						= 0;
	TRIS_GPIO4						= 0;
	TRIS_CONVST_EM					= 0;
	TRIS_RESET_EM					= 0;
	TRIS_AIR_Plus_Control			= 0;
	TRIS_AIR_Minus_Control			= 0;
	TRIS_PreChK_Control  			= 0;
	TRIS_DisChK_Control				= 0;
	TRIS_FansR_Control				= 0;
	TRIS_FansF_Control				= 0;
	TRIS_GPIO0						= 0;
	TRIS_GPIO1						= 0;
	TRIS_GPIO2						= 0;
	TRIS_LED3						= 0;
	TRIS_LED4						= 0;
	TRIS_LED5						= 0;

	TRIS_Det_IMD_latch				= 1;
	TRIS_Det_AMS_latch				= 1;
	TRIS_HV_Present_Sig				= 1;
	TRIS_Det_AIR_Plus				= 1;
	TRIS_Det_AIR_Minus				= 1;
	TRIS_Det_PreChK					= 1;
	TRIS_Det_DisChK					= 1;
	TRIS_PreCh_End_Sig				= 1;
	TRIS_Shutdown_Opening_Sig		= 1;
	//TRIS_BUSY_EM					= 1;
	TRIS_Shutdown_Above_Min			= 1;
	TRIS_Det_Shutdown_IMD_AMS		= 1;
	TRIS_Det_Shutdown_DCU_IMD		= 1;
	TRIS_Det_Shutdown_TSMS_Relays   = 1;
	TRIS_Det_Shutdown_AMS_DCU		= 1;
	TRIS_Det_IMD_OK					= 1;

	AMS_OK							= 0;
	GPIO3							= 0;
	GPIO4							= 0;
	AIR_Plus_Control				= 0;
	AIR_Minus_Control				= 0;
	PreChK_Control  				= 0;
	DisChK_Control					= 0;
	FansR_Control					= 0;
	FansF_Control					= 0;
	GPIO0							= 0;
	GPIO1							= 0;
	GPIO2							= 0;
	LED3							= 0;
	LED4							= 0;
	LED5							= 0;

	return;
}
