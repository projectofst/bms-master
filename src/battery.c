#include <stdint.h>
#include <stddef.h>

#include "battery.h"
#include "LTC6811/LTC6811.h"
#include "battery_monitor.h"
/**
 * @brief Initializes a battery structure
 */
void battery_init(Battery* battery){
    uint16_t stack_i, cell_i; // iterators
    Stack* stack; Cell* cell;

    /* init stack values */
    for(stack_i = 0; stack_i < BATTERY_SERIES_STACKS; ++stack_i){
        stack = &battery->stacks[stack_i];
        stack->id = stack_i;

        /* init cell values */
        for(cell_i = 0; cell_i < BATTERY_SERIES_CELLS_PER_STACK; ++cell_i){
            cell = &stack->cell_pack[cell_i];
            cell->id = stack_i*12 + cell_i;
            cell->voltage = UINT16_MAX;
            cell->temperature = INT16_MAX;
            cell->charge = UINT16_MAX;
            cell->state_of_charge = UINT16_MAX;
        }

        /* init slave values */
        stack->slave.discharge_channel_state = 0;
        stack->slave.discharge_channel_fault_mask = 0x0; //Assume OK
        stack->slave.cell_connection_fault_mask = 0x0000; // Assume OK
        stack->slave.temp_fault_mask = 0xFFFF; // Assume NOK
        stack->slave.slave_temperature[0] = INT16_MAX;
        stack->slave.slave_temperature[1] = INT16_MAX;
        stack->slave.heatsink_temperature[0] = INT16_MAX;
        stack->slave.heatsink_temperature[1] = INT16_MAX;
        stack->slave.BM_temperature = INT16_MAX;

        /* init battery monitor values */
        LTC6811_init(&stack->slave.BM);

        /* init stack summary values */
        stack->total_voltage = UINT16_MAX;
        stack->max_voltage = UINT16_MAX;
        stack->min_voltage = UINT16_MAX;
        stack->mean_voltage = UINT16_MAX;
        stack->max_voltage_cell = UINT8_MAX;
        stack->max_voltage_cell = UINT8_MAX;
        stack->min_voltage_cell = UINT8_MAX;
        stack->min_voltage_cell = UINT8_MAX;

        stack->state_of_charge = UINT16_MAX;

        stack->max_temperature = INT16_MAX;
        stack->min_temperature = INT16_MIN;
        stack->mean_temperature = 0;
        stack->max_temperature_cell = UINT8_MAX;
        stack->max_temperature_cell = UINT8_MAX;
        stack->min_temperature_cell = UINT8_MAX;
        stack->min_temperature_cell = UINT8_MAX;


    }

    /* slave communication */
    daisy_chain_B_init(&battery->daisy_chain_B);

    /* battery voltage values */
    battery->total_voltage = UINT16_MAX;
    battery->max_voltage = UINT16_MAX;
    battery->min_voltage = UINT16_MAX;
    battery->mean_voltage = UINT16_MAX;
    battery->max_voltage_cell[0] = UINT8_MAX;
    battery->max_voltage_cell[1] = UINT8_MAX;
    battery->min_voltage_cell[0] = UINT8_MAX;
    battery->min_voltage_cell[1] = UINT8_MAX;

    /* battery state of charge values */
    battery->state_of_charge = UINT16_MAX;

    /* battery temperature values */
    battery->max_temperature = INT16_MAX;
    battery->min_temperature = INT16_MIN;
    battery->mean_temperature = 0;
    battery->max_temperature_cell[0] = UINT8_MAX;
    battery->max_temperature_cell[1] = UINT8_MAX;
    battery->min_temperature_cell[0] = UINT8_MAX;
    battery->min_temperature_cell[1] = UINT8_MAX;

    /* battery current values */
    battery->current = 0;   // 0 is not invalid but current measurement is not
                            // working so let this be



    /* relay states */
    battery->relay_state.all = 1; // assume all relays start closed
    battery->relay_state.all = 0; // but the intended state is opened

    /* master status */
    battery->ams_ok = 0; // assume AMS is not OK
    battery->imd_ok = 0; // assume IMD is not OK
    battery->imd_latch = 0;
    battery->ams_latch = 0;
    battery->precharge = 1;
    battery->discharge = 1;
    battery->sc_dcu_imd = 0;
    battery->sc_imd_ams = 0;
    battery->sc_ams_dcu = 0;
    battery->sc_tsms_relays = 0;
    battery->shutdown_above_minimum = 0;
    battery->shutdown_open = 1;

    return;
}
