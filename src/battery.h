/* Include Guard */
#ifndef _BATTERY_H_
#define _BATTERY_H_

/*
 * Data structures representing the entire battery current state.
 */
#include <stdint.h>
#include <stdbool.h>

#include "LTC6811/LTC6811.h"

#include "can-ids/Devices/BMS_MASTER_CAN.h"

/**
 * @name Battery Electrical Configuration
 * @{
 */
#define BATTERY_SERIES_CELLS_PER_STACK  12
#define BATTERY_PARALLEL_CELLS  2
#define BATTERY_SERIES_STACKS   12
#define BATTERY_TOTAL_SERIES_CELLS  (BATTERY_SERIES_CELLS_PER_STACK * BATTERY_SERIES_STACKS)
#define BATTERY_TOTAL_CELLS (BATTERY_TOTAL_SERIES_CELLS * BATTERY_PARALLEL_CELLS)
/**@}*/

/**
 * @name Battery Operation Limits
 * @{
 */
#define CELL_LIMIT_MAX_T  570     /**< [ºC*0.1] */
#define CELL_LIMIT_MIN_T  0       /**< [ºC*0.1] */
#define STACK_LIMIT_MIN_CELL_NTC    8 /**< valid NTC per stack */
#define CELL_LIMIT_MAX_V      41900   /**< [mV*0.1] */
#define CELL_LIMIT_MIN_V      32000   /**< [mV*0.1] */
#define CELL_LIMIT_MAX_I_CHRG_CONT      1500    /**< [mA*10] */
#define CELL_LIMIT_MAX_I_CHRG           1500    /**< [mA*10] */
#define CELL_LIMIT_MAX_I_DISCH_CONT     10000   /**< [mA*10] */
#define CELL_LIMIT_MAX_I_DISCH          10000   /**< [mA*10] */
#define BAT_LIMIT_MAX_T     CELL_LIMIT_MAX_T
#define BAT_LIMIT_MIN_T     CELL_LIMIT_MIN_T
#define BAT_LIMIT_MAX_V     59900   /**< [mV*10] */
/** [mV*10] Minimum total battery voltage */
#define BAT_LIMIT_MIN_V     BATTERY_TOTAL_SERIES_CELLS * (CELL_LIMIT_MIN_V/100)
#define BAT_LIMIT_MAX_I_CHRG_CONT       -2500    /**< [mA*10] */
#define BAT_LIMIT_MAX_I_CHRG            -2500    /**< [mA*10] */
#define BAT_LIMIT_MAX_I_DISCH_CONT      12000   /**< [mA*10] */
#define BAT_LIMIT_MAX_I_DISCH           12000   /**< [mA*10] */

#define BAT_CHARGING_DELTA				50
#define SLAVE_MAX_HEATSINK_TEMP			750
/**@}*/

/**
 * @name Relay timings
 */
#define RELAY_BOUNCE_TIME 6
#define RELAY_OPERATION_TIME 25
#define RELAY_RELEASE_TIME 10

/**
 * @brief Structure representing a cell and its parameters
 */
typedef struct cell_struct{
    uint8_t id;    /**< cell ID within the whole battery */
    uint8_t voltage_state : 4;  /**< voltage status (OV/UV) */
    uint8_t temperature_state : 4;  /**< temperature status (OV/UV) */
    uint16_t voltage;   /**< [mV*0.1] current cell voltage */
    int16_t temperature;   /**< [ºC*0.1] current cell temperature */
    uint16_t charge;    /**< [mAh] estimated cell remaining charge */
    uint16_t state_of_charge;   /**< [%] estimated cell state of charge */
} Cell;

/**
 * @brief Structure representing a BMS slave
 */
typedef struct slave_struct{
    /**
     * Bitfield to keep what discharge channels are ON or OFF
     */
    uint16_t discharge_channel_state; /**< [ON/OFF] */

    /**
     * Bitfield to keep faulty discharge channels
     */
    uint16_t discharge_channel_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * Bitfiled to keep faulty cell connections
     * Basically if this is not 0 then the TS must be turned off
     */
    uint16_t cell_connection_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * Bitfield to keep faulty cell NTC
     */
    uint16_t temp_fault_mask; /**< [Fault - '1' / No fault - '0'] */

    /**
     * @name Temperatures in the Slave PCB
     * @{
     */
    int16_t slave_temperature[2];
    int16_t heatsink_temperature[2];
    int16_t BM_temperature;
    /**@}*/

    LTC6811 BM; /**< battery monitor information */
} Slave;

/**
 * @brief Structure representing a battery stack
 */
typedef struct stack_struct{
    uint8_t id;

    Cell cell_pack[BATTERY_SERIES_CELLS_PER_STACK]; /**< stack's cell group */
    Slave slave; /**< BMS Slave */

    uint16_t total_voltage; /**< [mV] stack's voltage */
    uint16_t max_voltage;   /**< [mV*0.1] voltage of the cell with the highest voltage */
    uint16_t min_voltage;   /**< [mV*0.1] voltage of the cell with the lowest voltage */
    uint16_t mean_voltage;  /**< [mv*0.1] mean of the stack's cell voltages */
    uint8_t max_voltage_cell;  /**< cell with the highest voltage */
    uint8_t min_voltage_cell;  /**< cell with the lowest voltage */

    /**
     * @brief State of charge of the entire cell pack
     *
     * The state of charge of the stack is defined as the state of charge of the
     * cell with the lowest state of charge within this stack.
     */
    uint16_t state_of_charge;

    int16_t max_temperature;   /**< [ºC*0.1] temperature of the hottest cell */
    int16_t min_temperature;   /**< [ºC*0.1] temperature of the coldest cell */
    int16_t mean_temperature;  /**< [ºC*0.1] mean of the stack's cell temperatures */
    int8_t max_temperature_cell;  /**< hottest cell */
    int8_t min_temperature_cell;  /**< coldest cell */
    uint16_t n_valid_cell_NTC; /**< number of valid cell NTC in this stack */
} Stack;

/**
 * @brief Structure representing the entire battery
 */
typedef struct battery_struct{
    Stack stacks[BATTERY_SERIES_STACKS];
    LTC6811_daisy_chain daisy_chain_B;

    uint16_t total_voltage; /**< [mV*10] battery's total voltage */
    uint16_t max_voltage;   /**< [mV*0.1] voltage of the cell with the highest voltage */
    uint16_t min_voltage;   /**< [mV*0.1] voltage of the cell with the lowest voltage */
    uint16_t mean_voltage;  /**< [mv*0.1] mean of the battery's cell voltages */
    uint8_t max_voltage_cell[2];  /**< cell with the highest voltage [stack, cell]*/
    uint8_t min_voltage_cell[2];  /**< cell with the lowest voltage [stack, cell]*/

    /**
     * @brief State of charge of the battery
     *
     * The state of charge of the battery is defined as the state of charge of the
     * cell with the lowest state of charge within the whole battery.
     */
    uint16_t state_of_charge;

    int16_t max_temperature;   /**< [ºC*0.1] temperature of the hottest cell */
    int16_t min_temperature;   /**< [ºC*0.1] temperature of the coldest cell */
    int16_t mean_temperature;  /**< [ºC*0.1] mean of the battery's cell temperatures */
    uint8_t max_temperature_cell[2];  /**< hottest cell [stack, cell]*/
    uint8_t min_temperature_cell[2];  /**< coldest cell [stack, cell]*/

    int16_t current;    /**< [mA*10] battery current */
    int16_t power;      /**< [W*10] battery power */

    union{
        struct{
            bool air_plus : 1;
            bool air_minus : 1;
            bool precharge_relay : 1;
            bool discharge_relay : 1;
        };
        uint16_t all;
    }relay_state;

    union{
        struct{
            bool air_plus : 1;
            bool air_minus : 1;
            bool precharge_relay : 1;
            bool discharge_relay : 1;
        };
        uint16_t all;
    }relay_intended_state;


	union {
		struct {
			bool ams_ok                 : 1;
			bool imd_ok                 : 1;
			bool imd_latch              : 1;
			bool ams_latch              : 1;
			bool air_positive			: 1;
			bool air_negative			: 1;
			bool precharge              : 1;
			bool discharge              : 1;
			bool sc_dcu_imd             : 1;
			bool sc_imd_ams             : 1;
			bool sc_ams_dcu             : 1;
			bool sc_tsms_relays         : 1;
			bool shutdown_above_minimum : 1;
			bool shutdown_open          : 1;
		};
		uint16_t status;
	};

	MASTER_MSG_TS_OFF_Reason ams_off_reason;

    /*
     * Missing structures for master PCB and HV monitor PCB
     */
} Battery;


/**
 * @brief Initializes a battery structure
 */
void battery_init(Battery* battery);

#endif /* End include guard */
