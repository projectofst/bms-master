#include <stdbool.h>
#include "lib_pic33e/utils.h"
#include "temperature.h"

/**
 * @brief Checks the validity of an NTC_connection
 *
 * @retval true  NTC connection is faulty
 * @retval false NTC connection is OK
 */
bool NTC_is_faulty(uint16_t voltage){
    // connection is open or shorted to V > MAX_VALID_TEMP_VOLT
    if (voltage > MAX_VALID_TEMP_VOLT){
        return true;

    // connection is shorted to ground or to V < MIN_VALID_TEMP_VOLT
    } else if (voltage < MIN_VALID_TEMP_VOLT){
        return true;
    } else {
        return false;
    }
}

/**
 * @brief Returns true if a NTC in a slave's fault mask is valid
 */
bool NTC_is_valid(uint16_t temp_fault_mask, uint16_t index){
    return ( ((temp_fault_mask >> index) & 1) == 0 );
}

/**
 * @brief Returns the number of valid NTC in a stack based on its
 * temp_fault_mask
 */
uint16_t  count_valid_cell_NTC_in_stack(uint16_t temp_fault_mask){
    return popcount( (~temp_fault_mask) & 0x0FFF );
}

