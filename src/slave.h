#ifndef __SLAVE_H__
#define __SLAVE_H__

/**
 * @brief Maps the mux address to the temp index
 *
 * The array is indexed by the mux address, contents are the index of the
 * temperature (as used in the temp_fault_mask bitfiled)
 */
const unsigned int NTC_mux_mapping[16] = {
    // mux 1
    3, 4, 5, 14, 2, 13, 1, 0,
    // mux 2
    10, 11, 15, 9, 8, 12, 7, 6
};

#endif
