#ifndef __MASTER_STATE
#define __MASTER_STATE

typedef struct _master_state {
	bool fans;
	bool verbose;
	bool charging;
	bool car_ts;
	bool ts_turn_on;
} MasterState;

#endif
