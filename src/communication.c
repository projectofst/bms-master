#include "communication.h"
#include <stdbool.h>
#include <stdint.h>
#include "lib_pic33e/can.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"
#include "can-ids/Devices/BMS_VERBOSE_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "battery.h"
#include "fifo.h"
#include "master_state.h"
#include "signals.h"
#include "ts_turn_on.h"

/*
 * Non-verbose messages
 */
CANdata compose_master_ts_message(bool state,
        MASTER_MSG_TS_OFF_Reason off_reason)
{
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_TS_STATE;
    msg.dlc = 2;
    msg.data[0] = ((off_reason << 1) | (state & 0b1));

    return msg;
}

CANdata compose_master_energy_meter_message(const Battery* battery){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_ENERGY_METER;
    msg.dlc = 8;
    msg.data[0] = battery->total_voltage;
    msg.data[1] = (uint16_t)battery->current;
    msg.data[2] = (uint16_t)battery->power;
    msg.data[3] = battery->state_of_charge;

    return msg;
}

CANdata compose_cell_voltage_info_message(const Battery* battery){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_CELL_VOLTAGE_INFO;
    msg.dlc = 8;
    msg.data[0] = battery->min_voltage;
    msg.data[1] = battery->mean_voltage;
    msg.data[2] = battery->max_voltage;
    msg.data[3] = ((battery->min_voltage_cell[1] << 8)
                    | battery->min_voltage_cell[0]) ;

    return msg;
}

CANdata compose_cell_temperature_info_message(const Battery* battery){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_CELL_TEMPERATURE_INFO;
    msg.dlc = 8;
    msg.data[0] = battery->min_temperature;
    msg.data[1] = battery->mean_temperature;
    msg.data[2] = battery->max_temperature;
    msg.data[3] = ((battery->max_temperature_cell[1] << 8)
                    | battery->max_temperature_cell[0]) ;

    return msg;
}

/* Master status message is composed elsewhere put it here */

CANdata compose_master_imd_error_message(){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_IMD_ERROR;
    msg.dlc = 0;

    return msg;
}

CANdata compose_master_ams_error_message(){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_AMS_ERROR;
    msg.dlc = 0;

    return msg;
}

CANdata compose_master_sc_open_message(){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_MASTER;
    msg.msg_id = MSG_ID_MASTER_SC_OPEN;
    msg.dlc = 0;

    return msg;
}
/*
 * Verbose messages
 */
CANdata compose_cell_info_message(const Cell* cell){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_VERBOSE;
    msg.msg_id = MSG_ID_MASTER_CELL_INFO;
    msg.dlc = 8;
    //msg.data[0] = ((cell->temperature_state << 12) | (cell->voltage_state << 8)
     //       | 1);
	msg.data[0] = cell->id;
    msg.data[1] = cell->voltage;
    msg.data[2] = (uint16_t)cell->temperature;
    msg.data[3] = cell->state_of_charge;

    return msg;
}


CANdata compose_slave_info_1_message(const Stack* stack){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_VERBOSE;
    msg.msg_id = MSG_ID_MASTER_SLAVE_INFO_1;
    msg.dlc = 8;
    msg.data[0] = stack->id;
    msg.data[1] = stack->slave.discharge_channel_fault_mask;
    msg.data[2] = stack->slave.cell_connection_fault_mask;
    msg.data[3] = stack->slave.temp_fault_mask;

    return msg;
}

CANdata compose_slave_info_2_message(const Stack* stack){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_VERBOSE;
    msg.msg_id = MSG_ID_MASTER_SLAVE_INFO_2;
    msg.dlc = 8;
    msg.data[0] = stack->id;
    msg.data[1] = stack->slave.discharge_channel_state;
    msg.data[2] = stack->slave.heatsink_temperature[0];
    msg.data[3] = stack->slave.heatsink_temperature[1];

    return msg;
}

CANdata compose_slave_info_3_message(const Stack* stack){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_VERBOSE;
    msg.msg_id = MSG_ID_MASTER_SLAVE_INFO_3;
    msg.dlc = 8;
    msg.data[0] = stack->id;
    msg.data[1] = stack->slave.slave_temperature[0];
    msg.data[2] = stack->slave.slave_temperature[1];
    msg.data[3] = stack->slave.BM_temperature;

    return msg;
}

#if 0
/* Debug message */
CANdata compose_slave_info_4_message(const Stack* stack){
    CANdata msg;

    msg.dev_id = DEVICE_ID_BMS_VERBOSE;
    msg.msg_id = MSG_ID_MASTER_SLAVE_INFO_4;
    msg.dlc = 8;
    msg.data[0] = stack->id;
    msg.data[1] = stack->n_valid_cell_NTC;
    msg.data[2] = stack->total_voltage;
    msg.data[3] = stack->mean_temperature;

    return msg;
}
#endif

void send_slave_and_cell_messages(const Battery* battery, Fifo* can_fifo){
    uint16_t slave, cell; // iterators
    CANdata msg;

    for(slave = 0; slave < BATTERY_SERIES_STACKS; ++slave){
        msg = compose_slave_info_1_message(&battery->stacks[slave]);
        append_fifo(can_fifo, msg);
        msg = compose_slave_info_2_message(&battery->stacks[slave]);
        append_fifo(can_fifo, msg);
        msg = compose_slave_info_3_message(&battery->stacks[slave]);
        append_fifo(can_fifo, msg);
#if 0
        msg = compose_slave_info_4_message(&battery->stacks[slave]);
        append_fifo(can_fifo, msg);
#endif


        for(cell = 0; cell < BATTERY_SERIES_CELLS_PER_STACK; ++cell){
            msg = compose_cell_info_message(&battery->stacks[slave].cell_pack[cell]);
            append_fifo(can_fifo, msg);
        }
    }

    return;
}

void send_battery_info_messages(const Battery* battery, Fifo* can_fifo){
    CANdata msg;

    msg = compose_master_energy_meter_message(battery);
    append_fifo(can_fifo, msg);
    msg = compose_cell_voltage_info_message(battery);
    append_fifo(can_fifo, msg);
    msg = compose_cell_temperature_info_message(battery);
    append_fifo(can_fifo, msg);

    return;
}


/* Build master status message */
void compose_status_message(CANdata *msg, Battery battery, MasterState master_state) {
	MASTER_MSG_Status status;
	msg->dev_id = DEVICE_ID_BMS_MASTER;
	msg->msg_id = MSG_ID_MASTER_STATUS;
	msg->dlc = 4;
	status.status = battery.status;
	status.verbose = master_state.verbose;
	status.ts_on = check_ts_on();
	// TODO: add ts to status
	msg->data[0] = status.status;
	msg->data[1] = PreCh_End_Sig;

	return;
}

void compose_charging_message(CANdata msg, MasterState master_state) {
	msg.dev_id = DEVICE_ID_BMS_MASTER;
	msg.msg_id = 43; //TODO: create message 

	msg.data[0] = 0;
	msg.data[1] = 0;
	msg.data[2] = 0;
	msg.data[3] = 0;

}


/* Handles two fifos. The verbose one and the normal communication channel
 * Should not write to the CAN TX buffer more than it can handle
 */
uint16_t can_dispatch(Fifo *fifo1, Fifo *fifo2) {
	CANdata *msg;

	uint16_t available = can1_tx_available();
	for (; available > 0 && !fifo_empty(fifo1); available--) {
		LED5 ^= 1;
		msg = pop_fifo(fifo1);
		if (msg == NULL) {
			/* Should never happen */
			break;;
		}
		send_can1(*msg);
	}

	for (; available > 0 && !fifo_empty(fifo2); available--) {
		msg = pop_fifo(fifo2);
		if (msg == NULL) {
			/* Nothing to send */
			break;
		}
		send_can1(*msg);
	}

	return !fifo_empty(fifo1) + !fifo_empty(fifo2);
}


int can_receive_handler(Battery *battery, MasterState *master_state, TS_TURN_ON *ts_turn_on_state) {
	CANdata msg;

	if (can1_rx_empty()) {
		return 0;
	}

	msg = pop_can1();

	if (msg.msg_id == CMD_ID_COMMON_TS) {
		LED3 ^= 1;
		master_state->car_ts ^= 1;
	}
	else if (msg.dev_id == DEVICE_ID_INTERFACE) {
		if (msg.msg_id == CMD_ID_INTERFACE_TOGGLE_BMS_VERBOSE) {
			LED3 ^= 1;
			master_state->verbose ^= 1;
		}
		else if (msg.msg_id == CMD_ID_INTERFACE_TOGGLE_BMS_FANS) {
			master_state->fans ^= 1;
		}
		else if (msg.msg_id == CMD_ID_INTERFACE_TOGGLE_BMS_CHARGING) {
			master_state->charging ^= 1;
		}
		else if (msg.msg_id == CMD_ID_INTERFACE_BMS_FAKE_ERROR) {
			battery->ams_ok=0;
			AMS_OK = 0;
		}
		else if (msg.msg_id == CMD_ID_INTERFACE_BMS_RESET_ERROR) {
			AMS_OK = 1;
			if (check_ams_ok()) {
				battery->ams_ok = 1;
			}
		}
	}

	return 1;
}
