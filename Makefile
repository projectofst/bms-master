# FST Lisboa
# Project Makefile

MCU := 33EP256MU806
PROGNAME := change_me

# Debug: -g, or empty to disable.
# Add extra flags here for debug conditional compilation, like -DDBG defining DBG
DBG := -g

# Optimization level: -Ox
# x = 0 (disabled), 1 (see MPLAB XC16 Compiler User's Guide 5.7.6 for all the options)
OPT := -O1

# Extra macros for conditional compilation (-D<...)
CPPFLAGS := -DNOUSB -DNOADC
#CPPFLAGS := -DFCY64

# External include directories
EXT_INCDIRS :=
# External library directories
EXT_LIBDIRS :=
# External libraries (-l<...>)
EXT_LIBS :=

###############################################################################
# Supposedly no more user editing is needed below
###############################################################################
CC := xc16-gcc

# Directories
SRCDIR := src/
BINDIR := bin/
OBJDIR := obj/
LIBDIR := lib/

SOURCES := $(shell find . -type f -name "*.c" | cut -d"/" -f2-)
OBJECTS := $(addprefix $(OBJDIR), $(SOURCES:.c=.o))

# Include the LIBDIR and SRCDIR directories.
CFLAGS := -I$(SRCDIR) -I$(LIBDIR)

# External includes
CFLAGS += $(foreach ext_incdir,$(EXT_INCDIRS),-I$(ext_incdir))
LDFLAGS := $(foreach ext_libdir,$(EXT_LIBDIRS),-L$(ext_libdir)) $(foreach ext_lib,$(EXT_LIBS),-l$(ext_lib))

CFLAGS += $(DBG) $(OPT) -mcpu=$(MCU) -msfr-warn=on -fast-math -pipe -merrata=all
# Warnings: see MPLAB XC16 Compiler User's Guide section 5.7.4 for the complete list
CFLAGS += -Wall -W -Wcast-align -Wcast-qual -Winline -Wpointer-arith -Wredundant-decls -Wshadow -mno-eds-warn 
# large stuff
CFLAGS +=  -mlarge-code -mlarge-arrays
#CFLAGS += -Wconversion -Wmissing-prototypes
CPPFLAGS += -MMD -MP
LDFLAGS += -Wl,--script=p$(MCU).gld,--warn-common,-Map="$(BINDIR)$(PROGNAME).map"

# Get number of logical cores, on Linux and on macOS.
OS := $(shell uname -s)
ifeq ($(OS),Darwin)
	NPROC := $(shell sysctl -n hw.ncpu)
else
	NPROC := $(shell nproc)
endif

.PHONY: init submodules verbose all flash clean
.DEFAULT_GOAL := all

init:
	rm -rf .git
	git init
	git remote add origin $$URL
	@ # Read submodules from .gitmodules. Assume this structure:
	@ # [submodule "lib/can-ids"]
	@ # 	branch = FST08e
	@ # 	url = git@gitlab.com:projectofst/can-ids
	@ # 	path = lib/can-ids
	# TODO: call add-submodules
	# TODO: call something else to .gitsubmodules
	git config -f .gitmodules -l | cut -d'=' -f2 | \
	while read branch; do read url; read path; \
	git submodule add -b $$branch $$url $$path; done
	make submodules
	git commit -m "Initial commit"

add-submodules:
	git config -f .gitmodules -l | cut -d'=' -f2 | \
	while read branch; do read url; read path; \
	git submodule add -b $$branch $$url $$path; done
	make submodules

submodules:
	git submodule init
	git submodule update --remote

verbose:
	VERBOSE=-Wl,--report-mem make

all:
	@ mkdir -p $(BINDIR) $(OBJDIR)
	make -j$(NPROC) $(BINDIR)$(PROGNAME).hex

flash: all
	/opt/microchip/mplabx/*/sys/java/*/bin/java -jar /opt/microchip/mplabx/*/mplab_ipe/ipecmd.jar -TPPK3 -P$(MCU) -M -F$(BINDIR)$(PROGNAME).hex
	@ rm -f log.[0-9] MPLABXLog.xml*

clean:
	rm -rf $(OBJDIR) $(BINDIR) || true

$(BINDIR)$(PROGNAME).hex: $(BINDIR)$(PROGNAME).elf
	xc16-bin2hex $(BINDIR)$(PROGNAME).elf

$(BINDIR)$(PROGNAME).elf: $(OBJECTS)
	$(CC) $(VERBOSE) $(LDFLAGS) -o $@ $^

$(OBJDIR)%.o: %.c
	@ mkdir -p `dirname $@`
	$(CC) -Wa,-a=$(@:.o=.s) -c $(CFLAGS) $(CPPFLAGS) -o $@ $<

-include $(OBJECTS:.o=.d)

# This rebuilds everything if the Makefile was modified
# http://stackoverflow.com/questions/3871444/making-all-rules-depend-on-the-makefile-itself/3892826#3892826
-include .dummy
.dummy: Makefile
	@ touch $@
	@ $(MAKE) -s clean
